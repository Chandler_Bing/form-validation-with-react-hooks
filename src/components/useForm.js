import React, { useState } from "react"

function useForm(initialState = {}, validate) {
  const [values, setValues] = useState(initialState)
  const [errors, setErrors] = useState({})
  const [submitting, setSubmitting] = useState(false)
  React.useEffect(() => {
    if (submitting) {
      setTimeout(() => setSubmitting(false), 2000)
    }
  }, [errors])

  const handleChange = e => {
    e.persist()
    setValues(values => ({ ...values, [e.target.name]: e.target.value }))
  }
  const handleSubmit = e => {
    e.persist()
    e.preventDefault()
    const validationErrors = validate(values)
    setErrors(validationErrors)
    setSubmitting(true)
  }
  const handleBlur = () => {
    const validationErrors = validate(values)
    setErrors(validationErrors)
  }
  return { values, handleChange, handleSubmit, errors, handleBlur, submitting }
}

export default useForm
