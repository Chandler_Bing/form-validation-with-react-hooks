import React from "react"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
import useForm from "./useForm"
import validate from "./validate"
import { Form, ErrorMessage } from "./styled"
const INITIAL_STATE = {
  email: "",
  password: "",
}
function form() {
  const {
    values,
    handleChange,
    handleSubmit,
    handleBlur,
    errors,
    submitting,
  } = useForm(INITIAL_STATE, validate)
  return (
    <Form onSubmit={handleSubmit}>
      <TextField
        label="Email"
        name="email"
        type="email"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.email}
        autoComplete="off"
      />
      {errors.email && <ErrorMessage>{errors.email}</ErrorMessage>}
      <TextField
        label="Password"
        name="password"
        type="password"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.password}
        autoComplete="off"
      />
      {errors.password && <ErrorMessage>{errors.password}</ErrorMessage>}

      <Button
        variant="contained"
        color={submitting ? "default" : "primary"}
        type="submit"
      >
        Submit{submitting && "ting"}
      </Button>
    </Form>
  )
}

export default form
