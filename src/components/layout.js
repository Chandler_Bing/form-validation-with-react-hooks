import React from "react"
import { Grid, Link } from "./styled"

export default function layout({ children }) {
  return (
    <Grid>
      <Link
        target="_blank"
        href="https://gitlab.com/Chandler_Bing/form-validation-with-react-hooks/tree/master/src/components"
      >
        Repo
      </Link>
      {children}
    </Grid>
  )
}
