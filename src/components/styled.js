import styled from "styled-components"
export const Grid = styled.section`
  display: grid;
  width: 100%;
  justify-items: center;
`
export const Form = styled.form`
  & > * {
    display: block !important;
    width: 100%;
  }
  button {
    margin-top: 25px;
  }
`
export const ErrorMessage = styled.p`
  color: red;
  margin: 0;
`
export const Link = styled.a`
  position: absolute;
  right: 15px;
  top: 15px;
`
